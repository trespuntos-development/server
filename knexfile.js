module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'root',
      password: 'masterkey',
      database: 'TresPuntosDev'
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }
}
