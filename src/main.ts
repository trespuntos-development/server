import 'source-map-support/register'
import Server from './server'

const server = new Server()
server.initialize()
