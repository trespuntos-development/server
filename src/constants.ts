export enum ErrorCodes {
    INTERAL_ERROR = 0,
    INVALID_PARAMS = 1,
    REGISTRATION_REQUIRED = 2,
    AUTH_FAIL = 3,
}
