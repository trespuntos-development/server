import * as bodyParser from 'body-parser'
import * as config from 'config'
import * as express from 'express'
import * as i18n from 'i18n'
import * as path from 'path'

import { appRouter } from './routes'
import { Logger } from './utils'

export default class Server {

    private readonly app: express.Application

    constructor() {
        this.app = express()
    }

    public initialize(): void {
        this.setupMiddlewares()
        this.setupRoutes()
        this.listen()
    }

    private setupMiddlewares(): void {
        this.app.use(bodyParser.urlencoded({ extended: true }))
        this.app.use(bodyParser.json())

        i18n.configure({
            defaultLocale: 'es',
            directory: path.resolve('./locales'),
            locales: ['es'],
        })
        this.app.use(i18n.init)
    }

    private setupRoutes(): void {
        this.app.use(appRouter)
    }

    private listen(): void {
        const port = config.get<number>('server.port')
        const pkg = require(path.resolve('./package.json'))
        const version = pkg.version
        this.app.listen(port, () => {
            Logger.info(`Server started at port ${port}. Running version ${version}.`)
        })
    }
}
