import * as bunyan from 'bunyan'

export const Logger = new bunyan({
    name: 'logger',
    src: true,
    streams: [
        {
            level: 'info',
            name: 'info',
            stream: process.stdout,
        },
        {
            level: 'error',
            name: 'error',
            stream: process.stderr,
        },
    ],
})
