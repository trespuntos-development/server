import { Collection } from 'bookshelf'
import { expect } from 'chai'
import { suite, test } from 'mocha-typescript'
import { signInWithFacebook, signInWithGoogle, signUp } from '../lib'
import { AlephCard, AlephCardHistoria, Cliente, IClienteAttributes } from '../models'

@suite
export class SignInSignUpTest {

    // Clean up test results
    public static after() {
        new Cliente()
            .fetchAll()
            .then((collection) => {
                return Promise.all(
                    collection.map((client) => client.destroy())
                )
            })

        new AlephCard()
            .fetchAll()
            .then((collection) => {
                return Promise.all(
                    collection.map((card) => card.destroy())
                )
            })

        new AlephCardHistoria()
            .fetchAll()
            .then((collection) => {
                return Promise.all(
                    collection.map((history) => history.destroy())
                )
            })
    }

    private static googleSignUp: Partial<IClienteAttributes> = {
        CORREO: 'odiaz.jose@gmail.com',
        GOOGLE_ID: '123456789',
        PRIMER_APELLIDO: 'Ortega',
        PRIMER_NOMBRE: 'Jose',
    }

    private static googleSignIn = {
        email: 'odiaz.jose@gmail.com',
        platformUserId: '123456789',
    }

    private static facebookSignUp: Partial<IClienteAttributes> = {
        CORREO: 'maxell2804@gmail.com',
        FACEBOOK_ID: '987456321',
        PRIMER_APELLIDO: 'Ortega',
        PRIMER_NOMBRE: 'Jose',
    }

    private static facebookSignIn = {
        email: 'maxell2804@gmail.com',
        platformUserId: '987456321',
    }

    @test
    public async 'It should register a new Client with Google auth'() {
        const client = await signUp(SignInSignUpTest.googleSignUp)

        expect(client).to.not.be.null

        if (client) {
            expect(client.id).to.be.greaterThan(0)
            expect(client.nickname).length.greaterThan(0)
            expect(client.primerApellido).length.greaterThan(0)
            expect(client.primerNombre).length.greaterThan(0)
            expect(client.googleId).length.greaterThan(0)

            await client.load('cards.history')
            const cards = client.related('cards') as Collection<AlephCard>
            expect(cards).length(1)

            const history = cards.first().related('history')
            expect(history).length(1)
        }
    }

    @test
    public async 'It should find a Client with Google auth'() {
        const client = await signInWithGoogle(SignInSignUpTest.googleSignIn)
        expect(client).to.not.be.null

        if (client !== null) {
            expect(client.tokenSesionWeb).length(30)
        }
    }

    @test
    public async 'It should register a new Client with Facebook auth'() {
        const client = await signUp(SignInSignUpTest.facebookSignUp)

        expect(client).to.not.be.null

        if (client) {
            expect(client.id).to.be.greaterThan(0)
            expect(client.nickname).length.greaterThan(0)
            expect(client.primerApellido).length.greaterThan(0)
            expect(client.primerNombre).length.greaterThan(0)
            expect(client.facebookId).length.greaterThan(0)

            await client.load('cards.history')
            const cards = client.related('cards') as Collection<AlephCard>
            expect(cards).length(1)

            const history = cards.first().related('history')
            expect(history).length(1)
        }
    }

    @test
    public async 'It should find a Client with Facebook auth'() {
        const client = await signInWithFacebook(SignInSignUpTest.facebookSignIn)
        expect(client).to.not.be.null

        if (client !== null) {
            expect(client.tokenSesionWeb).length(30)
        }
    }

}
