import * as bookshelfLib from 'bookshelf'
import * as config from 'config'
import * as knexLib from 'knex'

const knex = knexLib({
    client: 'mysql',
    connection: {
        database: config.get<string>('database.name'),
        host: config.get<string>('database.host'),
        password: config.get<string>('database.password'),
        port: config.get<number>('database.port'),
        user: config.get<string>('database.username'),
    },
    debug: config.get<boolean>('database.debug'),
    pool: {
        max: config.get<number>('database.pool.max'),
        min: config.get<number>('database.pool.min'),
    },
})

const bookshelf = bookshelfLib(knex)

export { knex, bookshelf }
