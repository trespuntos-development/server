import * as Bluebird from 'bluebird'
import { FetchAllOptions, ModelOptions, SaveOptions, SerializeOptions } from 'bookshelf'
import * as objectDiff from 'deep-object-diff'
import { bookshelf } from './connection'

export class BaseModel<T extends BaseModel<T, R>, R> extends bookshelf.Model<T> {

    constructor(attributes?: R, options?: ModelOptions) {
        super(attributes, options)
    }

    /**
     * Alias for `where` method with type assitance.
     *
     * @param properties
     */
    public whereAssisted(properties: R): T {
        return super.where(properties)
    }

    /**
     * Alias for `where` method with type assistance for a single field condition.
     *
     * @param field
     * @param valueOrOperator
     * @param valueIfOperator
     */
    public whereKeyVal(
        field: keyof R,
        valueOrOperator: string | number | boolean,
        valueIfOperator?: string | number | boolean
    ) {
        const fieldName = field.valueOf()

        if (valueIfOperator) {
            return super.where(fieldName, valueOrOperator, valueIfOperator)
        } else {
            return super.where(fieldName, valueOrOperator)
        }
    }

    public whereRaw(sql: string, ...bindings: any[]): T {
        return this.query((qb) => qb.whereRaw(sql, bindings))
    }

    public whereIn(field: keyof R, ...values: any[]): T {
        return this.whereRaw('?? IN (?)', field.valueOf(), values)
    }

    public whereNotIn(field: keyof R, ...values: any[]): T {
        return this.whereRaw('?? NOT IN (?)', field.valueOf(), values)
    }

    public andWhereRaw(sql: string, ...bindings: any[]): T {
        return this.query((qb) => qb.andWhereRaw(sql, bindings))
    }

    public orWhereRaw(sql: string, ...bindings: any[]): T {
        return this.query((qb) => qb.orWhereRaw(sql, bindings))
    }

    public limit(records: number, offset?: number): T {
        return this.query((qb) => {
            qb.limit(records)
            if (offset) {
                qb.offset(offset)
            }
        })
    }

    public columns(...names: Array<keyof R>): T {
        return this.query((qb) => qb.columns(names))
    }

    public toJSON(options?: SerializeOptions): R {
        return super.toJSON(options)
    }

    /**
     * Update all changes made to this model's attributes
     * since the model's last `fetch`, `refresh`, `save`
     * or `destroy`
     *
     * Warning: When updating a model created through the
     * model constructor, all attributes are considered as changes.
     *
     * @param {SaveOptions} options Bookshelf save options.
     */
    public update(options?: SaveOptions): Bluebird<T> {
        const diff = objectDiff.diff(this.previousAttributes(), this.attributes)
        return this.save(diff, { ...options, patch: true })
    }

    /**
     * Alias for `save` method with type assitance.
     *
     * @param attributes An object containing the attributes to save.
     * @param options Bookshelf save options.
     */
    public saveAssisted(attributes: R, options?: SaveOptions): Bluebird<T> {
        return this.save(attributes, options)
    }

    /**
     * Alias for `save` method with type assistance for a single field update.
     *
     * @param key Column name.
     * @param val Value to set.
     * @param options Bookshelf save options.
     */
    public saveKeyVal(key: keyof R, val: any, options?: SaveOptions): Bluebird<T> {
        return this.save(key.toString(), val, options)
    }

    public fetchAsPage(options?: IPaginationOptions): Bluebird<IPaginationResult<T>> {
        // Cast instance to any to prevent compile errors
        const instance: any = this
        return instance.fetchPage(options)
    }

}

interface IPaginationResult<T extends BaseModel<T, any>> {
    models: [T]
    pagination: {
        limit?: number
        offset?: number
        pageCount: number
        page?: number
        pageSize?: number
        rowCount: number
    }
}

interface IPaginationOptions extends FetchAllOptions {
    page?: number
    pageSize?: number
    limit?: number
    offset?: number
}

export default BaseModel
