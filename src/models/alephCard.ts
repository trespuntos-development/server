import { AlephCardHistoria, BaseModel } from './'

/**
 * Model that represents the table `TresPuntosAlephCard`.
 */
export class AlephCard extends BaseModel<AlephCard, IAlephCardAttributes> {

    public get tableName() { return 'TresPuntosAlephCard' }
    public get idAttribute() { return 'ID' }

    public history() {
        return this.hasMany(AlephCardHistoria, 'ALEPHCARD', 'ALEPHCARD')
    }

    public get codigoSistema(): number {
        return this.get('CODIGO_SISTEMA')
    }

    public set codigoSistema(value: number) {
        this.set('CODIGO_SISTEMA', value)
    }

    public get primerNombre(): string {
        return this.get('PRIMER_NOMBRE')
    }

    public set primerNombre(value: string) {
        this.set('PRIMER_NOMBRE', value)
    }

    public get primerApellido(): string {
        return this.get('PRIMER_APELLIDO')
    }

    public set primerApellido(value: string) {
        this.set('PRIMER_APELLIDO', value)
    }

    public get nickname(): string {
        return this.get('NICKNAME')
    }

    public set nickname(value: string) {
        this.set('NICKNAME', value)
    }

    public get cedula(): number {
        return this.get('CEDULA')
    }

    public set cedula(value: number) {
        this.set('CEDULA', value)
    }

    public get birthday(): Date {
        return this.get('BIRTHDAY')
    }

    public set birthday(value: Date) {
        this.set('BIRTHDAY', value)
    }

    public get asignada(): number {
        return this.get('ASIGNADA')
    }

    public set asignada(value: number) {
        this.set('ASIGNADA', value)
    }

    public get tipo(): string {
        return this.get('TIPO')
    }

    public set tipo(value: string) {
        this.set('TIPO', value)
    }

    public get puntos(): number {
        return this.get('PUNTOS')
    }

    public set puntos(value: number) {
        this.set('PUNTOS', value)
    }

    public get puntosHistorico(): number {
        return this.get('PUNTOS_HISTORICO')
    }

    public set puntosHistorico(value: number) {
        this.set('PUNTOS_HISTORICO', value)
    }

    public get nivel(): string {
        return this.get('NIVEL')
    }

    public set nivel(value: string) {
        this.set('NIVEL', value)
    }

    public get alephcard(): string {
        return this.get('ALEPHCARD')
    }

    public set alephcard(value: string) {
        this.set('ALEPHCARD', value)
    }

    public get codigoActivacion(): string {
        return this.get('CODIGO_ACTIVACION')
    }

    public set codigoActivacion(value: string) {
        this.set('CODIGO_ACTIVACION', value)
    }

    public get montoDisponible(): number {
        return this.get('MONTO_DISPONIBLE')
    }

    public set montoDisponible(value: number) {
        this.set('MONTO_DISPONIBLE', value)
    }

    public get idCliente(): number {
        return this.get('ID_CLIENTE')
    }

    public set idCliente(value: number) {
        this.set('ID_CLIENTE', value)
    }

}

export interface IAlephCardAttributes {
    ALEPHCARD?: string
    ASIGNADA?: number
    BIRTHDAY?: Date
    CEDULA?: number
    CODIGO_ACTIVACION?: string
    CODIGO_SISTEMA?: number
    ID?: number
    ID_CLIENTE?: number
    MONTO_DISPONIBLE?: number
    NICKNAME?: string
    NIVEL?: string
    PRIMER_APELLIDO?: string
    PRIMER_NOMBRE?: string
    PUNTOS?: number
    PUNTOS_HISTORICO?: number
    TIPO?: string
}
