import { AlephCard } from './'
import BaseModel from './baseModel'

/**
 * Model that represents the table `TestPuntosClientes`.
 */
export class Cliente extends BaseModel<Cliente, IClienteAttributes> {

    public get tableName() { return 'TresPuntosClientes' }
    public get idAttribute() { return 'ID' }

    public cards() {
        return this.hasMany(AlephCard, 'ID_CLIENTE')
    }

    public get correo(): string {
        return this.get('CORREO')
    }

    public set correo(value: string) {
        this.set('CORREO', value)
    }

    public get primerNombre(): string {
        return this.get('PRIMER_NOMBRE')
    }

    public set primerNombre(value: string) {
        this.set('PRIMER_NOMBRE', value)
    }

    public get primerApellido(): string {
        return this.get('PRIMER_APELLIDO')
    }

    public set primerApellido(value: string) {
        this.set('PRIMER_APELLIDO', value)
    }

    public get nickname(): string {
        return this.get('NICKNAME')
    }

    public set nickname(value: string) {
        this.set('NICKNAME', value)
    }

    public get googleId(): string {
        return this.get('GOOGLE_ID')
    }

    public set googleId(value: string) {
        this.set('GOOGLE_ID', value)
    }

    public get facebookId(): string {
        return this.get('FACEBOOK_ID')
    }

    public set facebookId(value: string) {
        this.set('FACEBOOK_ID', value)
    }

    public get fechaRegistro(): Date {
        return this.get('FECHA_REGISTRO')
    }

    public set fechaRegistro(value: Date) {
        this.set('FECHA_REGISTRO', value)
    }

    public get tokenSesionWeb(): string {
        return this.get('TOKEN_SESION_WEB')
    }

    public set tokenSesionWeb(value: string) {
        this.set('TOKEN_SESION_WEB', value)
    }

}

export interface IClienteAttributes {
    CORREO?: string
    FACEBOOK_ID?: string
    FECHA_REGISTRO?: Date
    GOOGLE_ID?: string
    ID?: any
    NICKNAME?: string
    PRIMER_APELLIDO?: string
    PRIMER_NOMBRE?: string
    TOKEN_SESION_WEB?: string
}
