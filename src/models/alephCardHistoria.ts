import BaseModel from './baseModel'

export class AlephCardHistoria extends BaseModel<AlephCardHistoria, IAlephCardHistoriaAttributes> {

    public get tableName() { return 'TresPuntosAlephCardHistoria' }
    public get idAttribute() { return 'ID' }

    public get transaccion(): string {
        return this.get('TRANSACCION')
    }

    public set transaccion(value: string) {
        this.set('TRANSACCION', value)
    }

    public get alephcard(): string {
        return this.get('ALEPHCARD')
    }

    public set alephcard(value: string) {
        this.set('ALEPHCARD', value)
    }

    public get tipo(): string {
        return this.get('TIPO')
    }

    public set tipo(value: string) {
        this.set('TIPO', value)
    }

    public get nivel(): string {
        return this.get('NIVEL')
    }

    public set nivel(value: string) {
        this.set('NIVEL', value)
    }

    public get primerNombre(): string {
        return this.get('PRIMER_NOMBRE')
    }

    public set primerNombre(value: string) {
        this.set('PRIMER_NOMBRE', value)
    }

    public get primerApellido(): string {
        return this.get('PRIMER_APELLIDO')
    }

    public set primerApellido(value: string) {
        this.set('PRIMER_APELLIDO', value)
    }

    public get cedula(): number {
        return this.get('CEDULA')
    }

    public set cedula(value: number) {
        this.set('CEDULA', value)
    }

    public get nicknameCliente(): string {
        return this.get('NICKNAME_CLIENTE')
    }

    public set nicknameCliente(value: string) {
        this.set('NICKNAME_CLIENTE', value)
    }

    public get birthday(): Date {
        return this.get('BIRTHDAY')
    }

    public set birthday(value: Date) {
        this.set('BIRTHDAY', value)
    }

    public get montoDisponible(): number {
        return this.get('MONTO_DISPONIBLE')
    }

    public set montoDisponible(value: number) {
        this.set('MONTO_DISPONIBLE', value)
    }

    public get montoPagar(): number {
        return this.get('MONTO_PAGAR')
    }

    public set montoPagar(value: number) {
        this.set('MONTO_PAGAR', value)
    }

    public get montoRecargado(): number {
        return this.get('MONTO_RECARGADO')
    }

    public set montoRecargado(value: number) {
        this.set('MONTO_RECARGADO', value)
    }

    public get nuevoMonto(): number {
        return this.get('NUEVO_MONTO')
    }

    public set nuevoMonto(value: number) {
        this.set('NUEVO_MONTO', value)
    }

    public get puntos(): number {
        return this.get('PUNTOS')
    }

    public set puntos(value: number) {
        this.set('PUNTOS', value)
    }

    public get puntosGenerados(): number {
        return this.get('PUNTOS_GENERADOS')
    }

    public set puntosGenerados(value: number) {
        this.set('PUNTOS_GENERADOS', value)
    }

    public get puntosUsado(): number {
        return this.get('PUNTOS_USADO')
    }

    public set puntosUsado(value: number) {
        this.set('PUNTOS_USADO', value)
    }

    public get nuevoPuntos(): number {
        return this.get('NUEVO_PUNTOS')
    }

    public set nuevoPuntos(value: number) {
        this.set('NUEVO_PUNTOS', value)
    }

    public get puntosHistorico(): number {
        return this.get('PUNTOS_HISTORICO')
    }

    public set puntosHistorico(value: number) {
        this.set('PUNTOS_HISTORICO', value)
    }

    public get productoRedimido(): string {
        return this.get('PRODUCTO_REDIMIDO')
    }

    public set productoRedimido(value: string) {
        this.set('PRODUCTO_REDIMIDO', value)
    }

    public get ultimos4(): string {
        return this.get('ULTIMOS4')
    }

    public set ultimos4(value: string) {
        this.set('ULTIMOS4', value)
    }

    public get cajero(): string {
        return this.get('CAJERO')
    }

    public set cajero(value: string) {
        this.set('CAJERO', value)
    }

    public get creada(): Date {
        return this.get('CREADA')
    }

    public set creada(value: Date) {
        this.set('CREADA', value)
    }

    public get actualizada(): Date {
        return this.get('ACTUALIZADA')
    }

    public set actualizada(value: Date) {
        this.set('ACTUALIZADA', value)
    }

    public get valida(): number {
        return this.get('VALIDA')
    }

    public set valida(value: number) {
        this.set('VALIDA', value)
    }

}

export interface IAlephCardHistoriaAttributes {
    ACTUALIZADA?: Date
    ALEPHCARD?: string
    BIRTHDAY?: Date
    CAJERO?: string
    CEDULA?: number
    CREADA?: Date
    ID?: number
    MONTO_DISPONIBLE?: number
    MONTO_PAGAR?: number
    MONTO_RECARGADO?: number
    NICKNAME_CLIENTE?: string
    NIVEL?: string
    NUEVO_MONTO?: number
    NUEVO_PUNTOS?: number
    PRIMER_APELLIDO?: string
    PRIMER_NOMBRE?: string
    PRODUCTO_REDIMIDO?: string
    PUNTOS?: number
    PUNTOS_GENERADOS?: number
    PUNTOS_HISTORICO?: number
    PUNTOS_USADO?: number
    TIPO?: string
    TRANSACCION?: string
    ULTIMOS4?: string
    VALIDA?: number
}
