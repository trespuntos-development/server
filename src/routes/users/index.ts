import { Router } from 'express'
import { sessionTokenAuthMiddleware } from '../../lib'
import {
    getHistoryScreenData,
    getHomeData,
    getPointsScreenData,
    getQRCode,
    signInUpWithFacebook,
    signInUpWithGoogle,
} from './handlers'

export const userRouter = Router()

userRouter.post('/signin/google', signInUpWithGoogle)
userRouter.post('/signin/facebook', signInUpWithFacebook)

userRouter.get('/qr', sessionTokenAuthMiddleware, getQRCode)
userRouter.get('/home', sessionTokenAuthMiddleware, getHomeData)
userRouter.get('/points', sessionTokenAuthMiddleware, getPointsScreenData)
userRouter.get('/history', sessionTokenAuthMiddleware, getHistoryScreenData)
