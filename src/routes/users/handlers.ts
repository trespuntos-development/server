import { Request, Response } from 'express'
import * as fs from 'fs-extra'
import * as path from 'path'
import * as qrcode from 'qrcode'
import { ErrorCodes } from '../../constants'
import {
    errorResponse,
    IAuthenticatedRequest,
    signInWithFacebook,
    signInWithGoogle,
    signUp,
    successResponse,
} from '../../lib'
import { AlephCard, AlephCardHistoria, Cliente, IClienteAttributes } from '../../models'
import { Logger } from '../../utils'

export async function signInUpWithGoogle(req: Request, res: Response) {
    genericSignInUp('google', req, res)
}

export async function signInUpWithFacebook(req: Request, res: Response) {
    genericSignInUp('facebook', req, res)
}

type Intents = 'sign-in' | 'sign-up'
type SignInPlatforms = 'google' | 'facebook'

async function genericSignInUp(signInUpPlatform: SignInPlatforms, req: Request, res: Response) {
    const intent: Intents = req.body.intent
    const email: string = req.body.email
    const name: string = req.body.name
    const lastname: string = req.body.lastname
    const nickname: string = req.body.nickname || name
    let platformUserId: string

    switch (signInUpPlatform) {
        case 'google':
            platformUserId = req.body.google_id
            break
        case 'facebook':
            platformUserId = req.body.facebook_id
            break
        default:
            throw new Error('Invalid platform')
    }

    if (
        !intent ||
        (intent === 'sign-in' && !(email && platformUserId)) ||
        (intent === 'sign-up' && !(email && platformUserId && name && lastname))
    ) {
        res.json(errorResponse({
            code: ErrorCodes.INVALID_PARAMS,
            message: req.__('ERROR:INVALID_PARAMS'),
        }))
        return
    }

    if (intent === 'sign-in') {
        let user: Cliente | null

        if (signInUpPlatform === 'google') {
            user = await signInWithGoogle({ email, platformUserId })
        } else {
            user = await signInWithFacebook({ email, platformUserId })
        }

        if (user === null) {
            res.json(errorResponse({
                code: ErrorCodes.REGISTRATION_REQUIRED,
                message: req.__('ERROR:USER_NOT_REGISTERED', { email }),
            }))
            return
        }

        res.json(successResponse({
            message: req.__('SUCCESS:LOGGED_IN'),
            session_token: user.tokenSesionWeb,
        }))
    } else if (intent === 'sign-up') {
        const signUpParams: Partial<IClienteAttributes> = {
            CORREO: email,
            NICKNAME: nickname,
            PRIMER_APELLIDO: lastname,
            PRIMER_NOMBRE: name,
        }

        if (signInUpPlatform === 'google') {
            signUpParams.GOOGLE_ID = platformUserId
        } else {
            signUpParams.FACEBOOK_ID = platformUserId
        }

        const registeredUser = await signUp(signUpParams)
        if (registeredUser === null) {
            const internalErrorResponse = errorResponse({
                code: ErrorCodes.INTERAL_ERROR,
                message: req.__('ERROR:INTERNAL_ERROR'),
            })
            res.json(internalErrorResponse)
            return
        }

        res.json(successResponse({
            message: req.__('SUCCESS:USER_REGISTERED'),
            session_token: registeredUser.tokenSesionWeb,
        }))
    } else {
        res.json(errorResponse({
            code: ErrorCodes.INTERAL_ERROR,
            message: req.__('ERROR:INVALID_ACTION'),
        }))
    }
}

export async function getHomeData(req: IAuthenticatedRequest, res: Response) {
    const client = req.authenticatedClient

    const digitalCard = await new AlephCard()
        .whereAssisted({ ID_CLIENTE: client.id, TIPO: 'DIGITALC' })
        .fetch()

    const cardHistory = await new AlephCardHistoria()
        .whereAssisted({ ALEPHCARD: digitalCard.alephcard })
        .orderBy('CREADA', 'DESC')
        .fetch()

    res.json(successResponse({
        money: cardHistory.nuevoMonto,
        points: cardHistory.nuevoPuntos,
    }))
}

export async function getPointsScreenData(req: IAuthenticatedRequest, res: Response) {
    const client = req.authenticatedClient

    const digitalCard = await new AlephCard()
        .whereAssisted({ ID_CLIENTE: client.id, TIPO: 'DIGITALC' })
        .fetch()

    const cardHistory = await new AlephCardHistoria()
        .whereAssisted({ ALEPHCARD: digitalCard.alephcard })
        .orderBy('CREADA', 'DESC')
        .fetch()

    const pointsRecord = await new AlephCardHistoria()
        .whereAssisted({ ALEPHCARD: digitalCard.alephcard })
        .whereRaw('YEAR(CREADA) = ?', new Date().getFullYear())
        .orderBy('NUEVO_PUNTOS', 'DESC')
        .fetch()
        .then((record) => {
            if (record) {
                return record.nuevoPuntos
            } else {
                return 0
            }
        })

    res.json(successResponse({
        points: cardHistory.nuevoPuntos,
        points_record: pointsRecord,
    }))
}

export async function getQRCode(req: IAuthenticatedRequest, res: Response) {
    const client = req.authenticatedClient

    const digitalCard = await new AlephCard()
        .whereAssisted({ ID_CLIENTE: client.id, TIPO: 'DIGITALC' })
        .fetch()

    const file = `${digitalCard.alephcard}.svg`
    const filePath = path.resolve(`./codes/${file}`)
    qrcode.toFile(filePath, digitalCard.alephcard, { errorCorrectionLevel: 'medium' }, async (qrError) => {
        if (qrError) {
            Logger.error(qrError)
            res.json(errorResponse({
                code: ErrorCodes.INTERAL_ERROR,
                message: req.__('ERROR:INTERNAL_ERROR'),
            }))
            return
        }

        res.sendFile(filePath, async (error) => {
            if (error) {
                Logger.error(error)
            }

            try {
                await fs.unlink(filePath)
            } catch (error) {
                Logger.warn(error)
            }
        })
    })
}

export async function getHistoryScreenData(req: IAuthenticatedRequest, res: Response) {
    const limit: number = req.body.limit || 15
    const client = req.authenticatedClient

    const digitalCard = await new AlephCard()
        .whereAssisted({ ID_CLIENTE: client.id, TIPO: 'DIGITALC' })
        .fetch()

    const cardHistory = await new AlephCardHistoria()
        .whereAssisted({ ALEPHCARD: digitalCard.alephcard })
        .whereKeyVal('TRANSACCION', '!=', 'Activacion') // Hide activation history
        .orderBy('CREADA', 'DESC')
        .limit(limit)
        .fetchAll()

    const historyCollection = cardHistory.map((history) => {
        return {
            date: history.creada,
            money: history.montoDisponible,
            money_after: history.nuevoMonto,
            money_paid: history.montoPagar,
            money_recharged: history.montoRecargado,
            points: history.puntos,
            points_after: history.nuevoPuntos,
            points_generated: history.puntosGenerados,
            points_used: history.puntosUsado,
        }
    })

    res.json(successResponse({
        history: historyCollection,
    }))
}
