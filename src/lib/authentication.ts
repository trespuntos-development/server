import { Cliente } from '../models'
import { Logger } from '../utils'

export async function athenticateWithSessionToken(sessionToken: string): Promise<Cliente | null> {
    try {
        const user = await new Cliente()
            .whereAssisted({ TOKEN_SESION_WEB: sessionToken })
            .fetch({ require: true })

        return user
    } catch (error) {
        if ('EmptyResponse' !== error.message) {
            Logger.error(error)
        }

        return null
    }
}
