import * as randomString from 'randomstring'
import { AlephCard, AlephCardHistoria, Cliente, IClienteAttributes } from '../models'
import { Logger } from '../utils'

export async function signUp(params: IClienteAttributes): Promise<Cliente | null> {
    try {
        if (!params.NICKNAME) {
            params.NICKNAME = params.PRIMER_NOMBRE
        }

        params.TOKEN_SESION_WEB = randomString.generate({
            capitalization: 'uppercase',
            length: 30,
        })

        const user = new Cliente(params)
        await user.save()
        await createClientDigitalCard(user)
        return user
    } catch (error) {
        Logger.error(error)
        return null
    }
}

async function generateUniqueCardCode(length: number = 10): Promise<string> {
    const code = randomString.generate({
        capitalization: 'uppercase',
        length,
    })

    const codeCount = await new AlephCard()
        .whereAssisted({ ALEPHCARD: code })
        .count()

    if (codeCount > 0) {
        return generateUniqueCardCode()
    } else {
        return code
    }
}

async function createClientDigitalCard(client: Cliente): Promise<AlephCard> {
    const card = new AlephCard({
        ALEPHCARD: await generateUniqueCardCode(),
        ASIGNADA: 3,
        CEDULA: 0,
        CODIGO_ACTIVACION: '00000',
        CODIGO_SISTEMA: 0,
        ID_CLIENTE: client.id,
        MONTO_DISPONIBLE: 0,
        NICKNAME: client.nickname,
        NIVEL: 'Blanco',
        PRIMER_APELLIDO: client.primerApellido,
        PRIMER_NOMBRE: client.primerNombre,
        PUNTOS: 0,
        PUNTOS_HISTORICO: 0,
        TIPO: 'DIGITALC',
    })

    await card.save()
    await createCardActivationHistory(card)
    return card
}

async function createCardActivationHistory(card: AlephCard): Promise<AlephCardHistoria> {
    const activationHistory = await new AlephCardHistoria({
        ACTUALIZADA: new Date(),
        ALEPHCARD: card.alephcard,
        CAJERO: 'SISTEMA',
        CEDULA: card.cedula,
        CREADA: new Date(),
        MONTO_DISPONIBLE: 0,
        MONTO_PAGAR: 0,
        MONTO_RECARGADO: 0,
        NICKNAME_CLIENTE: card.nickname,
        NIVEL: card.nivel,
        NUEVO_MONTO: 0,
        NUEVO_PUNTOS: 0,
        PRIMER_APELLIDO: card.primerApellido,
        PRIMER_NOMBRE: card.primerNombre,
        PRODUCTO_REDIMIDO: '',
        PUNTOS: 0,
        PUNTOS_GENERADOS: 0,
        PUNTOS_HISTORICO: 0,
        PUNTOS_USADO: 0,
        TIPO: '',
        TRANSACCION: 'Activacion',
        ULTIMOS4: 'ACT',
        VALIDA: 0,
    })

    await activationHistory.save()
    return activationHistory
}
