import { NextFunction, Request, Response } from 'express'
import { ErrorCodes } from '../constants'
import { Cliente } from '../models'
import { athenticateWithSessionToken, errorResponse } from './'

export async function sessionTokenAuthMiddleware(req: Request, res: Response, next: NextFunction) {
    const sessionToken: string = req.body.session_token || req.query.session_token
    const client = await athenticateWithSessionToken(sessionToken)

    if (client === null) {
        res.json(errorResponse({
            code: ErrorCodes.AUTH_FAIL,
            message: req.__('ERROR:AUTH_FAIL'),
        }))
    } else {
        Object.defineProperty(req, 'authenticatedClient', {
            configurable: false,
            enumerable: true,
            value: client,
            writable: false,
        })
        next()
    }
}

export interface IAuthenticatedRequest extends Request {
    authenticatedClient: Cliente
}
