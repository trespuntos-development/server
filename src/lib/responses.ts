export function successResponse(content: ISuccess): IResponse {
    return {
        content,
        success: true,
    } as IResponse
}

export function errorResponse(error: IError): IResponse {
    return {
        error,
        success: false,
    } as IResponse
}

interface IResponse {
    success: boolean
    content?: ISuccess
    error?: IError
}

interface ISuccess {
    [key: string]: any
    message?: string
}

interface IError {
    message: string
    code: number
}
