import * as randomString from 'randomstring'
import { Cliente } from '../models'
import { Logger } from '../utils'

export async function signInWithGoogle(params: ISignInParams): Promise<Cliente | null> {
    try {
        const user = await new Cliente()
            .whereAssisted({ CORREO: params.email, GOOGLE_ID: params.platformUserId })
            .fetch({ require: true })

        user.tokenSesionWeb = randomString.generate({
            capitalization: 'uppercase',
            length: 30,
        })
        await user.update()

        return user
    } catch (error) {
        if ('EmptyResponse' !== error.message) {
            Logger.error(error)
        }
        return null
    }
}

export async function signInWithFacebook(params: ISignInParams): Promise<Cliente | null> {
    try {
        const user = await new Cliente()
            .whereAssisted({ CORREO: params.email, FACEBOOK_ID: params.platformUserId })
            .fetch({ require: true })

        user.tokenSesionWeb = randomString.generate({
            capitalization: 'uppercase',
            length: 30,
        })
        await user.update()

        return user
    } catch (error) {
        if ('EmptyResponse' !== error.message) {
            Logger.error(error)
        }
        return null
    }
}

interface ISignInParams {
    email: string
    platformUserId: string
}
