
exports.up = function(knex, Promise) {
  const AlephCardTable = knex.raw(`
    CREATE TABLE TresPuntosAlephCard (
        ID int(11) NOT NULL AUTO_INCREMENT,
        CODIGO_SISTEMA int(11) DEFAULT NULL,
        PRIMER_NOMBRE varchar(40) CHARACTER SET utf8 DEFAULT NULL,
        PRIMER_APELLIDO varchar(40) CHARACTER SET utf8 DEFAULT NULL,
        NICKNAME varchar(40) CHARACTER SET utf8 DEFAULT NULL,
        CEDULA int(11) DEFAULT NULL,
        BIRTHDAY date NOT NULL,
        ASIGNADA int(11) DEFAULT NULL,
        TIPO varchar(8) CHARACTER SET utf8 DEFAULT NULL,
        PUNTOS int(11) DEFAULT NULL,
        PUNTOS_HISTORICO int(11) NOT NULL,
        NIVEL varchar(12) COLLATE utf8_spanish_ci NOT NULL,
        ALEPHCARD varchar(10) CHARACTER SET utf8 DEFAULT NULL,
        CODIGO_ACTIVACION varchar(5) COLLATE utf8_spanish_ci NOT NULL,
        MONTO_DISPONIBLE int(11) DEFAULT NULL,
        PRIMARY KEY (ID),
        UNIQUE KEY ID (ID)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;  
  `)

  const AlephCardHistoriaTable = knex.raw(`
    CREATE TABLE TresPuntosAlephCardHistoria (
      ID int(11) NOT NULL AUTO_INCREMENT,
      TRANSACCION varchar(30) COLLATE utf8_spanish_ci NOT NULL,
      ALEPHCARD varchar(12) COLLATE utf8_spanish_ci NOT NULL,
      TIPO varchar(30) COLLATE utf8_spanish_ci NOT NULL,
      NIVEL varchar(12) COLLATE utf8_spanish_ci NOT NULL,
      PRIMER_NOMBRE varchar(60) COLLATE utf8_spanish_ci NOT NULL,
      PRIMER_APELLIDO varchar(60) COLLATE utf8_spanish_ci NOT NULL,
      CEDULA int(15) NOT NULL,
      NICKNAME_CLIENTE varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
      BIRTHDAY date NOT NULL,
      MONTO_DISPONIBLE int(11) DEFAULT NULL,
      MONTO_PAGAR int(11) DEFAULT NULL,
      MONTO_RECARGADO int(11) NOT NULL,
      NUEVO_MONTO int(11) DEFAULT NULL,
      PUNTOS int(11) DEFAULT NULL,
      PUNTOS_GENERADOS int(11) DEFAULT NULL,
      PUNTOS_USADO int(11) NOT NULL,
      NUEVO_PUNTOS int(11) NOT NULL,
      PUNTOS_HISTORICO int(9) NOT NULL,
      PRODUCTO_REDIMIDO varchar(600) COLLATE utf8_spanish_ci NOT NULL,
      ULTIMOS4 varchar(4) COLLATE utf8_spanish_ci NOT NULL,
      CAJERO varchar(80) COLLATE utf8_spanish_ci DEFAULT NULL,
      CREADA datetime NOT NULL,
      ACTUALIZADA timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
      VALIDA int(1) NOT NULL,
      PRIMARY KEY (ID)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;  
  `)

  const ClientTable = knex.schema.createTable('TresPuntosClientes', t => {
    t.increments('ID')
    t.string('CORREO').notNullable()
    t.string('PRIMER_NOMBRE').notNullable()
    t.string('PRIMER_APELLIDO').notNullable()
    t.string('NICKNAME').notNullable()
    t.string('GOOGLE_ID').defaultTo('')
    t.string('FACEBOOK_ID').defaultTo('')
    t.timestamp('FECHA_REGISTRO').defaultTo(knex.fn.now())
    t.string('TOKEN_SESION_WEB').defaultTo('')

    t.unique('CORREO')
  })

  return Promise.all([AlephCardTable, AlephCardHistoriaTable, ClientTable])
}

exports.down = function(knex, Promise) {
  const dropAlephCardTable = knex.schema.dropTable('TresPuntosAlephCard')
  const dropAlephCardHistoriaTable = knex.schema.dropTable('TresPuntosAlephCardHistoria')
  const dropClientesTable = knex.schema.dropTable('TresPuntosClientes')
  return Promise.all([dropAlephCardTable, dropAlephCardHistoriaTable, dropClientesTable])
}
