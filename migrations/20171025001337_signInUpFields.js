
exports.up = function(knex, Promise) {
  const modifyAlephCard = knex.schema.table('TresPuntosAlephCard', t => {
    t.integer('ID_CLIENTE')
  })

  return Promise.all([modifyAlephCard])
}

exports.down = function(knex, Promise) {
  const modifyAlephCard = knex.schema.table('TresPuntosAlephCard', t => {
    t.dropColumns('ID_CLIENTE')
  })

  return Promise.all([modifyAlephCard])
}
