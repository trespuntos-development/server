![Tres Puntos](https://pbs.twimg.com/profile_images/733373589202870272/K9pFsp1P.jpg)

# Tres Puntos Server

## Setup

Install dependencies
```
npm install
```

Start server
```
npm start
```

Build project
```
npm run build
```

## API

* Login
    * Email
    * Google
    * Facebook
* Register
    * Email
    * Google
    * Facebook
* Forgot password
* Home
* QR Code
* Points
* History

## Version

NodeJS version: v8.7.0

Typescript version: v2.5.3

Typescript ES target: ES2017

## Resources
[Postman Collection](http://localhost)